package main

import (
	"fmt"
	"time"
)

func main() {
	loc, err := time.LoadLocation("Asia/Seoul")
	if err != nil {
		panic(err)
	}

	start := time.Date(2011, time.June, 20, 10, 0, 0, 0, loc)
	end := time.Date(2015, time.September, 17, 19, 0, 0, 0, loc)
	fmt.Println("start:\t", start)
	fmt.Println("end:\t", end)

	dur := end.Sub(start).Hours()
	days := dur / 24
	years := days / 365

	fmt.Printf("total:\t %v days (%v years)", days, years)
}
