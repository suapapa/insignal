# Insignal: suapapa@insignal.co.kr

20110620-20150915

* Linux committer
* GDG Korea Golang 운영자
* AOSP committer
* Made http://git.insignal.co.kr
* English to Korean translator of 처음 시작하는 인텔 갈릴레오, 메트 리처드슨 지음/이호민 옮김, 한빛미디어

# Reference
* [InSignal](http://www.insignal.co.kr/)
